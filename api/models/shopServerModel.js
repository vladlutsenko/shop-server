'use strict';
var mongoose = require('mongoose');
var Schema = mongoose.Schema;


var ProductSchema = new Schema({
  item: {
    type: String
  },
  src: {
    type: String
  },
  price: {
    type: Number
  },
  id: {
    type: Number
  }
});

module.exports = mongoose.model('products', ProductSchema);